<?php
// Start the session
session_start();

// Check if the cart session variable is set
if(!isset($_SESSION['cart'])) {
    // If it's not set, create an empty array
    $_SESSION['cart'] = array();
}

// Get the product id from the query string
$product_id = $_GET['id'];

// Add the product to the cart
$_SESSION['cart'][] = $product_id;

// Redirect the user to the cart page
header('Location: cart_zalogowany.php');
exit;
?>