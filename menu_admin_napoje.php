<?php
session_start();
if (!isset($_SESSION['admin']))
	{
		header('Location: menu.php');
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="style.css" rel="stylesheet">
	</head>
	<body><div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
				<a href="index_admin.php" >Home</a>
					<a href="menu_admin.php" class="active">Menu</a>
                    <a href="dodaj_admin.php">Dodaj produkt</a>
                    <a href="wyloguj.php">Wyloguj <?php echo $_SESSION['email']; ?></a> 
				</div>
				</header>
	<div class="menu_content">
				<div class="tab" id="Tab">
					<a href="menu_admin_burger.php" id="Burgery" >Burgery</a>
					<a href="menu_admin_vegeburger.php" id="BurgeryVege" >Burgery Vege</a>
					<a href="menu_admin_sosy.php" id="Sosy" >Sosy</a>
					<a href="menu_admin_napoje.php" id="Napoje">Napoje</a>
				</div>
				<div id="BurgeryTab">
					<table>
						<?php
						$db = new mysqli('localhost', 'root', '', 'burgerownia');
						$produkt="SELECT * FROM produkty WHERE typ='napoj'";
						$result = $db->query($produkt);

						// Czy zapytanie zwróciło jakieś wyniki
						if ($result->num_rows > 0) {
							echo '<table>';
    
							while ($row = $result->fetch_assoc()) {
								echo '<tr>';
								echo '<td colspan="3">';
								echo '<h3>' . $row['name'] . '</h3>';
								echo '<p>' . $row['description'] . '</p>';
								echo '</td>';
								echo '<td>' . $row['price'] . ' zł<br /><a href="delete_produkt.php?id=' . $row['id'] . '">Usuń produkt</a></td>';
								echo '</tr>';
							}
							echo '</table>';
						}
						else
						{
							echo 'Brak dostępnych produktów';
						}
						$db->close();
						?>					
					</table>
				</div>
			</div>

			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>








