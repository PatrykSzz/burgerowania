<?php
session_start();
if (!isset($_SESSION['admin']))
	{
		header('Location: index.php');
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />		
		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
				<a href="index_admin.php" >Home</a>
					<a href="menu_admin.php" >Menu</a>
                    <a href="dodaj_admin.php"class="active">Dodaj produkt</a>
                    <a href="wyloguj.php">Wyloguj <?php echo $_SESSION['email']; ?></a> 
				</div>
			</header>
			<div class="content">
				<div class="text">
                <h1>Tutaj możesz dodać <b>nowy produkt</b>!</h1>
                <form action="add_produkt.php" method="post" enctype="multipart/form-data">
                <label for="name">Nazwa produktu:</label><br>
                <input type="text" id="name" name="name"><br>
                <br>
                <label for="price">Cena:</label><br>
                <input type="text" id="price" name="price"><br>
                <br>
                <label for="description">Opis:</label><br>
                <textarea id="description" name="description" rows="10"></textarea><br>
                <br>
				<label for="typ">Typ produktu:</label><br>
				<select name='typ'>
                    <option value='burger'>Burger</option>
                    <option value='vegeburger'>Vege burger</option>
					<option value='napoj'>Napoj</option>
					<option value='sos'>Sos</option>
                </select><br>
                <input type="submit" value="Dodaj produkt">
                </form> 
				</div>
				<div class="image"><img src="images/burger.png"></div>
			</div>
			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>

