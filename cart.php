<?php

	session_start();
	
	if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true))
	{
		header('Location: cart_zalogowany.php');
		exit();
	}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Royal Burgers</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<header>
				<div class="logo"><img src="images/logo.png"></div>
				<div class="menu">
					<a href="index.php">Home</a>
					<a href="menu.php">Menu</a>
					<a href="cart.php" class="active">Koszyk</a>
					<a href="zaloguj.php">Zaloguj</a>
				</div>
			</header>
			<div class="cart_content">
				<div class="order">
					<h4>Proszę się <a href = "zaloguj.php">zalogować</a></h4></br><h4>Zamówienia można składać będąc zalogowanym</h4>
				</div>
			</div>
			<footer>
				<div>ul. Przemysłowa 20,<br />61-872 Poznań</div>
				<div><b>Godziny otwarcia:</b><br />Pn-Nd 11:00-23:00</div>
				<div><b>Telefon:</b> 800 758 749<br /><b>E-mail:</b> rburgers@gmail.com
			</footer>
		</div>
	</body>
</html>